self.addEventListener("install", function (event) {

	event.waitUntil(
		caches.open("henouser").then(function (cache) {
			cache.addAll([
				"./",
				"./style.css",
				"./Dosis-Book.ttf",
				"./mountain.jpg"
			]);
		})
	);

	self.skipWaiting();

});

self.addEventListener("fetch", function (event) {

	event.respondWith(

		caches.match(event.request).then(function (response) {
			if (response) {
				return response;
			} else {
				return fetch(event.request).then(function(response) {
					return response;
				});
			}
		})

	);

});